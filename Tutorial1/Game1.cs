﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Tutorial1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Texture2D background;
        private Texture2D pogMaya;
        private Texture2D lul;
        private AnimatedSprite animatedSprite;

        private SpriteFont font;
        private double score = 0;
        private float angle = 0;

        private Vector2 MayaLoc = new Vector2(0, 0);
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            background = Content.Load<Texture2D>("background");
            pogMaya = Content.Load<Texture2D>("PogMaya");
            lul = Content.Load<Texture2D>("LUL");

            Texture2D animatedTexture = Content.Load<Texture2D>("SmileyWalk");
            animatedSprite = new AnimatedSprite(animatedTexture, 4, 4);

            font = Content.Load<SpriteFont>("ScoreFont");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            score += gameTime.ElapsedGameTime.TotalSeconds;
            animatedSprite.Update();
            angle += 0.01f;

            KeyboardState state = Keyboard.GetState();

            if (state.IsKeyDown(Keys.Left))
                MayaLoc.X -= 10;
            if (state.IsKeyDown(Keys.Right))
                MayaLoc.X += 10;
            if (state.IsKeyDown(Keys.Up))
                MayaLoc.Y -= 10;
            if (state.IsKeyDown(Keys.Down))
                MayaLoc.Y += 10;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            spriteBatch.Draw(background, new Rectangle(0, 0, 800, 480), Color.White);
            spriteBatch.Draw(pogMaya, new Vector2(0,0), new Rectangle(0,0,600,600), Color.Pink, angle, new Vector2(300,600), 1.0f, SpriteEffects.None, 1);
            spriteBatch.Draw(lul, MayaLoc, Color.White);
            spriteBatch.DrawString(font, "Score: " + score, new Vector2(100, 100), Color.Yellow);

            animatedSprite.Draw(spriteBatch, new Vector2(300, 300));

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
